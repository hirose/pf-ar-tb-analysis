#include<iostream>

#include"TFile.h"
#include"TTree.h"

Int_t class_maker(void){

  std::cout<<"Example of using MakeClass to make an analysis skeleton"<<std::endl;

  //Defining the ROOT file object
  std::string filename = "../data/run4675_conv.root";
  TFile* root_file = new TFile(filename.c_str());
  root_file->ls();

  //Retrieving your tree from the ROOT file
  TTree *tree = root_file->Get<TTree>("EventTree");
  tree->Print(0);
  tree->Show(0);

  //Below generates a file EventTree.C/.h containing a definition of an analysis class.
  tree->MakeClass();

  return 0;
}
