//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Dec 21 15:31:17 2023 by ROOT version 6.28/04
// from TTree EventTree/
// found on file: ../data/run4675_conv.root
//////////////////////////////////////////////////////////

#ifndef EventTree_h
#define EventTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class EventTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           nFADC;
   Int_t           nSamples;
   Int_t           EventNo;
   Int_t           HeaderError;
   Short_t         Data125[20][16][64];
   Short_t         Data500[20][16][256];
   Float_t         Pedestal[20][16];
   Int_t           Peak[20][16];
   Int_t           Time[20][16];
   Float_t         CFTime[20][16];
   Float_t         EventSum[20][16];
   Int_t           EtSum_FADC125[20][64];
   Int_t           EtSum_FADC500[20][256];
   Long64_t        BufferLoopNo;
   Int_t           Error[20];
   Int_t           TimeStamp[20];
   Int_t           TrigNo[20];
   Int_t           SpillNo[20];
   Int_t           SlotNo[20];
   Int_t           Compression_flag[20][16];
   Int_t           Is500MHz[20];

   // List of branches
   TBranch        *b_nFADC;   //!
   TBranch        *b_nSamples;   //!
   TBranch        *b_EventNo;   //!
   TBranch        *b_HeaderError;   //!
   TBranch        *b_Data125;   //!
   TBranch        *b_Data500;   //!
   TBranch        *b_Pedestal;   //!
   TBranch        *b_Peak;   //!
   TBranch        *b_Time;   //!
   TBranch        *b_CFTime;   //!
   TBranch        *b_EventSum;   //!
   TBranch        *b_EtSum_FADC125;   //!
   TBranch        *b_EtSum_FADC500;   //!
   TBranch        *b_BufferLoopNo;   //!
   TBranch        *b_Error;   //!
   TBranch        *b_TimeStamp;   //!
   TBranch        *b_TrigNo;   //!
   TBranch        *b_SpillNo;   //!
   TBranch        *b_SlotNo;   //!
   TBranch        *b_Compression_flag;   //!
   TBranch        *b_Is500MHz;   //!

   EventTree(TTree *tree=0);
   virtual ~EventTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

   //Custom Method Added
   void DrawHist(TH1* hist, std::string drawoption, Bool_t logy, Bool_t logz);

};

#endif

#ifdef EventTree_cxx
EventTree::EventTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../data/run4675_conv.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../data/run4675_conv.root");
      }
      f->GetObject("EventTree",tree);

   }
   Init(tree);
}

EventTree::~EventTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t EventTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t EventTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void EventTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("nFADC", &nFADC, &b_nFADC);
   fChain->SetBranchAddress("nSamples", &nSamples, &b_nSamples);
   fChain->SetBranchAddress("EventNo", &EventNo, &b_EventNo);
   fChain->SetBranchAddress("HeaderError", &HeaderError, &b_HeaderError);
   fChain->SetBranchAddress("Data125", Data125, &b_Data125);
   fChain->SetBranchAddress("Data500", Data500, &b_Data500);
   fChain->SetBranchAddress("Pedestal", Pedestal, &b_Pedestal);
   fChain->SetBranchAddress("Peak", Peak, &b_Peak);
   fChain->SetBranchAddress("Time", Time, &b_Time);
   fChain->SetBranchAddress("CFTime", CFTime, &b_CFTime);
   fChain->SetBranchAddress("EventSum", EventSum, &b_EventSum);
   fChain->SetBranchAddress("EtSum_FADC125", EtSum_FADC125, &b_EtSum_FADC125);
   fChain->SetBranchAddress("EtSum_FADC500", EtSum_FADC500, &b_EtSum_FADC500);
   fChain->SetBranchAddress("BufferLoopNo", &BufferLoopNo, &b_BufferLoopNo);
   fChain->SetBranchAddress("Error", Error, &b_Error);
   fChain->SetBranchAddress("TimeStamp", TimeStamp, &b_TimeStamp);
   fChain->SetBranchAddress("TrigNo", TrigNo, &b_TrigNo);
   fChain->SetBranchAddress("SpillNo", SpillNo, &b_SpillNo);
   fChain->SetBranchAddress("SlotNo", SlotNo, &b_SlotNo);
   fChain->SetBranchAddress("Compression_flag", Compression_flag, &b_Compression_flag);
   fChain->SetBranchAddress("Is500MHz", Is500MHz, &b_Is500MHz);
   Notify();
}

Bool_t EventTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void EventTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t EventTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   if(false) std::cout<<entry<<std::endl; //Added to remove warnings...
   return 1;
}
#endif // #ifdef EventTree_cxx
